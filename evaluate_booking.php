<?php
	require_once('connect.php');
	openHeader();
	echo '    <title>XTV Online Booking - Evaluating Booking</title>';
	closeHeader();
	
	//check if times are valid
	if (!(isTime2Later($_POST['time_start'], $_POST['time_end']))){
		echo 'Your end time has to be later than your start time.<br><a href="javascript:history.back()">Try again</a>';
		$booking_valid = false;
	}
	//check if date entered
	elseif(strlen($_POST['date_booking']) < 10){
		echo "Make sure you've picked a date.".'<br><a href="javascript:history.back()">Try again</a>';
		$booking_valid = false;
	}
	//check if equipment not booked yet
	else
	{
		$timestamp1 = strtotime($_POST['date_booking']." ".$_POST['time_start']);
		$timestamp2 = strtotime($_POST['date_booking']." ".$_POST['time_end']);
		foreach($_POST['equipment'] as $equipmentid){
			if (isEquipmentBooked($equipmentid, $timestamp1, $timestamp2)){
				echo 'The equipment you have requested has already been booked at the times specified.<br>Please check the bookings, and <a href="javascript:history.back()">try again</a>.';
				$booking_valid = false;
				break;
			}
			else {
				$booking_valid = true;
			}
		}
	}
		
	if ($booking_valid == true) {
		//create booking
		createBooking(getUserID(), $_POST['title'], $_POST['date_booking'], $_POST['time_start'], $_POST['time_end'], $_POST['equipment']);
		//email to notify admin
			$subject = 'New Booking to Approve';
			$message = 'Hi, '.getFirstName($email_admin_id).PHP_EOL."there's a new booking for you to approve:".PHP_EOL."By: ".getFullName(getUserID())." (".getEmail(getUserID()).")".PHP_EOL."For: ".$_POST['title'].PHP_EOL.PHP_EOL."Please check the online booking system for more details and to approve the booking.";
			
			if (!wp_mail(getEmail($email_admin_id), $subject, $message)){
				echo "mail notification failed";
			}
	
		//show details of booking
		echo "Your Booking:<br />";
		echo "Booking Date: ".$_POST['date_booking']."<br />";
		echo "Booking Times: ".$_POST['time_start']." - ".$_POST['time_end']."<br />Equipment booked: ";
		foreach ($_POST['equipment'] as $id){
			$name = getEquipmentName($id);
			echo "$name".", ";
		}
	} //end if/else

 	getFooter();
?>