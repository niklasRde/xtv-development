<?php
	require_once('connect.php');
	openHeader();
	echo '    <title>XTV Online Booking- Approve Bookings</title>';
	closeHeader();
	
	//require user to be admin as set in config.php
	foreach($admin_ids as $id){
			if(getUserID() == $id){
				$authorized = true;
				break;
			}
		}
	if (!isset($authorized)){
		die("Sorry, ".getFirstName(getUserID()).", you are not authorized to approve bookings. Please ask the Administrator to get a booking approved.");
	}
?>
<a href="" target="_blank">Open in new tab</a><br />
Ordered by creation of booking<br /><table border="1"><tr><th style="width:1em;">ID</th><th>TITLE</th><th style="width:7em;">APPROVE</th><th>Date Booked</th><th style="width:6em;">Date of Booking</th><th style="width:4em;">From</th><th style="width:4em;">Until</th><th style="width:7em;">By</th><th>Equipment Booked</th></tr>

<?php
			
	//get unapproved bookings
		$sql = "SELECT * FROM `bookings_bookings` WHERE `approved` = 0 ORDER BY `date_booked` ASC";
		$result = mysql_query( $sql );
		
		//LOOP OVER DATA AND EXTRACT DATA
		while($row = mysql_fetch_array($result))
			{
				echo '<tr><td class="autowidth">';
				echo $row['id'];
				echo '</td><td class="autowidth">';
				echo $row['title'];
				echo '</td><td class="autowidth">';
				?>
             	<form action="evaluate_approval.php" method="post" onsubmit="return confirm('Do you really want to do this?');">
        			<div class="buttonHolder">
                        <input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
                        <input type="submit" name="action" value="Approve" /><br /><br />
                        <input type="submit" name="action" value="Delete"/>
                  </div>
             	</form>  
				<?php
				echo '</td><td class="autowidth">';
				echo $row['date_booked'];
				echo '</td><td class="autowidth">';
				echo $row['date_booking'];
				echo '</td><td class="autowidth">';
				echo $row['time_start'];
				echo '</td><td class="autowidth">';
				echo $row['time_end'];
				echo '</td><td class="autowidth">';
				echo getFullName($row['booked_by']);
				echo '</td><td class="autowidth">';
				echo getEquipmentBookedPlain($row['id']);
				echo "</td>";
			}
			
			//insert empty row to distinguish between approved/non-approved bookings
			echo '<tr><td class="autowidth"></td><td class="autowidth"></td><td class="autowidth" style="font-weight:bold;">ALREADY APPROVED</td><td class="autowidth"></td><td class="autowidth"></td><td class="autowidth"></td><td class="autowidth"></td><td class="autowidth"></td><td class="autowidth"></td></tr>';
			
			
	//get approved bookings (for deletion/deapproval)
		$sql = "SELECT * FROM `bookings_bookings` WHERE `approved` = 1 ORDER BY `date_booked` ASC";
		$result = mysql_query( $sql );
		
		//LOOP OVER DATA AND EXTRACT DATA
		while($row = mysql_fetch_array($result))
			{
				echo'<tr><td class="autowidth">';
				echo $row['id'];
				echo'</td><td class="autowidth">';
				echo $row['title'];
				echo'</td><td class="autowidth">';
				?>
             	<form action="evaluate_approval.php" method="post" onsubmit="return confirm('Do you really want to do this?');">
                	<div class="buttonHolder">
                        <input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
                        <input type="submit" name="action" value="Disapprove" /><br /><br />
                        <input type="submit" name="action" value="Delete"/>
                  </div>
             	</form>  
				<?php
				echo'</td><td class="autowidth">';
				echo $row['date_booked'];
				echo'</td><td class="autowidth">';
				echo $row['date_booking'];
				echo'</td><td class="autowidth">';
				echo $row['time_start'];
				echo'</td><td class="autowidth">';
				echo $row['time_end'];
				echo'</td><td class="autowidth">';
				echo getFullName($row['booked_by']);
				echo'</td><td class="autowidth">';
				echo getEquipmentBookedPlain($row['id']);
				echo "</td>";
			}
	
		echo'</table>';

 	getFooter();
?>