<?php
	require_once('connect.php');
	openHeader();
?>
    <!--Custom Header for this file-->
		<title>XTV Online Booking - Add Booking</title>
       	<link rel="stylesheet" type="text/css" media="all" href="jsDatePick_ltr.min.css" />
       	<script type="text/javascript" src="jsDatePick.min.1.3.js"></script>
       	<script type="text/javascript">
       		window.onload = function(){
           	new JsDatePick({
              		useMode:2,
                  target:"date",
                  dateFormat:"%Y-%m-%d",
				});
			};
		</script>
    <!--Custom Header Done-->
<?php
	closeHeader();
	
	
	//category (0 for everything, 1 for production, 2 for post-production)
		if (isset($_GET['c']) and ($_GET['c'] <= 2) and ($_GET['c'] >= 0)) {
				$category = $_GET['c'];
		}
		else{
			$category = 0;
		}
		
		//limits
		switch ($category) {
			case 0:
				$show = "";
				break;
			case 1:
				$show = "WHERE (`group` < 11) OR (`group` > 14) ";
				break;
			case 2:
				$show = "WHERE (`group` > 11) AND (`group` <= 14)";
				break;
		}	
		
?>
<form action="evaluate_booking.php" method="post" >
	Project:
   	<input type="text" name="title" /><br />
   	Date:
   	<input type="text" id="date" name="date_booking" /><br />
   	Time Start:
   	<select name="time_start" size="1">
    	<option value="05:00:00">05:00</option>
       	<option value="05:30:00">05:30</option>
       	<option value="06:00:00">06:00</option>
       	<option value="06:30:00">06:30</option>
       	<option value="07:00:00">07:00</option>
       	<option value="07:30:00">07:30</option>
       	<option value="08:00:00">08:00</option>
       	<option value="08:30:00">08:30</option>
    	<option value="09:00:00">09:00</option>
       	<option value="09:30:00">09:30</option>
       	<option value="10:00:00">10:00</option>
       	<option value="10:30:00">10:30</option>
       	<option value="11:00:00">11:00</option>
       	<option value="11:30:00">11:30</option>
       	<option value="12:00:00">12:00</option>
       	<option value="12:30:00">12:30</option>
       	<option value="13:00:00">13:00</option>
       	<option value="13:30:00">13:30</option>
       	<option value="14:00:00">14:00</option>
       	<option value="14:30:00">14:30</option>
       	<option value="15:00:00">15:00</option>
       	<option value="15:30:00">15:30</option>
       	<option value="16:00:00">16:00</option>
       	<option value="16:30:00">16:30</option>
       	<option value="17:00:00">17:00</option>
       	<option value="17:30:00">17:30</option>
       	<option value="18:00:00">18:00</option>
       	<option value="18:30:00">18:30</option>
       	<option value="19:00:00">19:00</option>
       	<option value="19:30:00">19:30</option>
       	<option value="20:00:00">20:00</option>
       	<option value="20:30:00">20:30</option>
       	<option value="21:00:00">21:00</option>
       	<option value="21:30:00">21:30</option>
       	<option value="22:00:00">22:00</option>
       	<option value="22:30:00">22:30</option>
       	<option value="23:00:00">23:00</option>
    </select>
    <br />Time End:
    <select name="time_end" size="1">
       	<option value="05:30:00">05:30</option>
       	<option value="06:00:00">06:00</option>
       	<option value="06:30:00">06:30</option>
       	<option value="07:00:00">07:00</option>
       	<option value="07:30:00">07:30</option>
       	<option value="08:00:00">08:00</option>
       	<option value="08:30:00">08:30</option>
    	<option value="09:00:00">09:00</option>
       	<option value="09:30:00">09:30</option>
       	<option value="10:00:00">10:00</option>
       	<option value="10:30:00">10:30</option>
       	<option value="11:00:00">11:00</option>
       	<option value="11:30:00">11:30</option>
       	<option value="12:00:00">12:00</option>
       	<option value="12:30:00">12:30</option>
       	<option value="13:00:00">13:00</option>
       	<option value="13:30:00">13:30</option>
       	<option value="14:00:00">14:00</option>
       	<option value="14:30:00">14:30</option>
       	<option value="15:00:00">15:00</option>
       	<option value="15:30:00">15:30</option>
       	<option value="16:00:00">16:00</option>
       	<option value="16:30:00">16:30</option>
       	<option value="17:00:00">17:00</option>
       	<option value="17:30:00">17:30</option>
       	<option value="18:00:00">18:00</option>
       	<option value="18:30:00">18:30</option>
       	<option value="19:00:00">19:00</option>
       	<option value="19:30:00">19:30</option>
       	<option value="20:00:00">20:00</option>
       	<option value="20:30:00">20:30</option>
       	<option value="21:00:00">21:00</option>
       	<option value="21:30:00">21:30</option>
       	<option value="22:00:00">22:00</option>
       	<option value="22:30:00">22:30</option>
       	<option value="23:00:00">23:00</option>
       	<option value="23:30:00">23:30</option>
    </select>
    <br /><br />
    Equipment needed:
    <select name="equipment[]" size="10" multiple>
    <?php
    //GET DATA FROM TABLE
    $sql = "SELECT * FROM bookings_equipment $show";
    $result = mysql_query( $sql );
    
    //LOOP OVER DATA AND EXTRACT IDs
    while($row = mysql_fetch_array($result))
        {
            echo '<option value="';
            echo $row['id']; //VALUE
            echo '">';					
            echo $row['name']; //OPTION
            echo " (".getGroupName($row['group']).")";
			 echo'</option>';
        }
    ?>
    </select>(Select multiple items by holding Ctrl/Cmd/Strg)<br />
    <input type="submit" value="Enter" />
</form>	

<?php
 	getFooter();
?>