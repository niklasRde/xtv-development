<?php
	require_once('connect.php');
	openHeader();
	echo '    <title>XTV Online Booking - Bookings Weekview</title>';
	closeHeader();
	echo '<a href="" target="_blank">Open in new tab</a><br />';
	
	//get dates
		//if custom week/year set use that, otherwise use current week/year
		if (isset($_GET['w']) && ($_GET['w'] < 53) && ($_GET['w'] > 0)) {
			$week = $_GET['w'];
		}
		else{
			$week = date("W");
		}
		if (isset($_GET['y']) && ($_GET['y'] < 2037) && ($_GET['y'] > 1970)) {
			$year = $_GET['y'];
		}
		else{
			$year = date("Y");
		}
		//set start date as Monday of current week
		$day = mktime( 0, 0, 0, 1, 1,  $year ) + ((7+1-(date( 'N', mktime( 0, 0, 0, 1, 1,  $year ) )))*86400) + ($week-2)*7*86400 + 1 ;
		$days = 14; //number of days shown
		
	//category (0 for everything, 1 for production, 2 for post-production)
		if (isset($_GET['c']) and ($_GET['c'] <= 2) and ($_GET['c'] >= 0)) {
				$category = $_GET['c'];
		}
		else{
			$category = 0;
		}
		
		//limits
		switch ($category) {
			case 0:
				$show = "";
				break;
			case 1:
				$show = "WHERE (`group` < 11) OR (`group` > 14) ";
				break;
			case 2:
				$show = "WHERE (`group` > 11) AND (`group` <= 14)";
				break;
		}	
		
	
	//show links for next/previous week
		if (($week > 1) && ($week < 52)){
			echo '<a href="?y='.$year.'&amp;w='.($week-1).'&amp;c='.$category.'">Previous Week</a> | <a href="?y='.$year.'&amp;w='.($week+1).'&amp;c='.$category.'">Next Week</a>';
		}
		elseif($week == 1){
			echo '<a href="?y='.($year-1).'&amp;c='.$category.'&amp;w=52">Previous Week</a> | <a href="?y='.$year.'&amp;c='.$category.'&amp;w='.($week+1).'">Next Week</a>';
		}
		elseif($week == 52){
			echo '<a href="?y='.$year.'&amp;c='.$category.'&amp;w='.($week-1).'">Previous Week</a> | <a href="?y='.($year+1).'&amp;c='.$category.'&amp;w=1">Next Week</a>';
		}
		
	//link to current week
		echo ' | <a href="?c='.$category.'">Current Week</a><br /><br />';
		
		
	//Start table output
		echo '<table>';
		echo '<tr><th>Date</th>';
	
	//GET DATA FROM TABLE
		$sql = "SELECT * FROM bookings_equipment $show ORDER BY `group` ASC";
		$result = mysql_query( $sql );
		
		//LOOP OVER DATA AND EXTRACT IDs
		while($row = mysql_fetch_array($result))
			{
				echo "<th>";
				echo $row['name'];
				echo "</th>";
			}
		echo "</tr>"; //DONE HEADING
	
	//show group of equipment
		echo "<tr><th>Group</th>";
		$sql = "SELECT * FROM bookings_equipment $show ORDER BY `group` ASC";
		$result = mysql_query( $sql );
		
		//LOOP OVER DATA AND get group
		while($row = mysql_fetch_array($result))
			{
				echo "<td>";
				echo getGroupName($row['group']);
				echo"</td>";
			}
		echo "</tr>"; //DONE HEADING
		
	//look for bookings in date range supplied (see above)
		for ($i = 1; $i <=$days; $i++) {
			//start new row
			echo "<tr>";
			//date
			echo "<th>".date("l, d.m.Y", $day)."</th>";
			
			//get booked slots for equipment
			$sql = "SELECT * FROM bookings_equipment $show";
			$result = mysql_query( $sql );
			
			//LOOP OVER DATA AND EXTRACT IDs
			while($row = mysql_fetch_array($result))
				{
					echo "<td>";
					$equipment_id = $row['id'];
					//check if equipment is booked on day
					if (isEquipmentBookedOnDay($day, $equipment_id)){
						foreach(getEquipmentBookings($day, $equipment_id) as $booking_id){
							if (isBookingApproved($booking_id)){
								print '<div style="text-align: center; font-weight: bold; display: block; margin-left: auto; margin-right: auto;">'.getBookingTitle($booking_id)."</div>Booked from ".substr(getTimeBookedFrom($booking_id), 0, 5)." until ".substr(getTimeBookedUntil($booking_id), 0, 5)." by ".getFullName(getBookedUserID($booking_id))."<br />";
							}
							else {
								print '<div style="text-align: center; font-weight: bold; display: block; margin-left: auto; margin-right: auto;">'.getBookingTitle($booking_id)."</div>Requested from ".substr(getTimeBookedFrom($booking_id), 0, 5)." until ".substr(getTimeBookedUntil($booking_id), 0, 5)." by ".getFullName(getBookedUserID($booking_id))."<br />";
							}
						}
					}
					else{
					print "";
					}
					
					echo"</td>";
				}

			//close row
			echo "</tr>";
			
			$day = increaseTimestampByADay($day);
		}
		
	echo '</table>'; //close table
 	getFooter(); //close page
?>