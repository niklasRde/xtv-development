<?php
	require_once('connect.php');
	openHeader();
	echo '    <title>XTV Online Booking - Evaluating Approval</title>';
	closeHeader();
	
	//require user to be admin as set in config.php
	foreach($admin_ids as $id){
			if (getUserID() == $id){
				$authorized = true;
				break;
			}
		}
	if (!$authorized) {die("Not authorized");};
	
	$bookedByID = getBookedByID($_POST['id']);
	$bookingTitle = getBookingTitle($_POST['id']);
	$bookingDate = getBookingDate($_POST['id']);
		
	//evaluate input	and take action in database
	if($_POST['action'] == 'Approve'){
		approveBooking($_POST['id']);
		$action = 'approved';
	}
	
	elseif($_POST['action'] == 'Delete'){
		deleteBooking($_POST['id']);
		$action = 'deleted';
	}
	
	elseif($_POST['action'] == 'Disapprove'){
		disapproveBooking($_POST['id']);
		$action = 'disapproved';
	};
	
	//show admin what happened.
	echo "Booking ID".$_POST['id']." has been ".$action.".";
	
	//email booker
	$subject = "Your booking has been ".$action;
	$message = "Hi ".getFirstName($bookedByID).",".PHP_EOL."Your booking for ".$bookingTitle." on ".$bookingDate ." has been ".$action.".";
	$email_address = getEmail($bookedByID);
	
	if (!wp_mail($email_address, $subject, $message)){ //sent email and if wp_mail() fails,..
			echo "mail notification failed";	//show error
		}

	echo'<br /> <a href="approve_bookings.php">Back to Approvals</a><br />'; //link back to approvals page for admin
 	getFooter();
?>