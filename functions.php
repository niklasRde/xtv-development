<?php
require_once('../wp-config.php');

function openHeader(){
	echo 
		'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'.PHP_EOL. 
		'<html xmlns="http://www.w3.org/1999/xhtml">'.PHP_EOL.
		'<head>'.PHP_EOL.
		'    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'.PHP_EOL.
		'    <link rel="stylesheet" type="text/css" href="style.css" />'.PHP_EOL;
	}
	
function closeHeader(){
	echo 
		'</head>'.PHP_EOL.
		'<body>
		';
	}
	
function getFooter(){
	echo 
		'</body>'.PHP_EOL.
		'</html>
		';
}

function currentPageURL() {
	$pageURL = 'http';
	//if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
	$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
	$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

function getUserID(){
	return wp_get_current_user()->ID;
}

function forceLogIn(){ //reedirects to wordpress-login when not logged in.
	if ( !is_user_logged_in() ) { 
		$redirect_to = urlencode(currentPageURL());
		$home = get_home_url();
		header( 'Location:'.$home.'/wp-login.php?redirect_to='.$redirect_to ) ;
	}
}

function createBooking($userid, $title, $date, $timestart, $timeend, $equipment){
	$timenow = date('Y-m-d H:i:s');
	//create new booking
	mysql_query("
		INSERT INTO bookings_bookings ( 
			title , date_booked , date_booking , time_start , time_end , booked_by	
		) 
		VALUES(
			'$title', '$timenow', '$date', '$timestart', '$timeend', '$userid'
		)
		");
	$bookingid = mysql_insert_id();
	
	//Insert booked equipment into relevant table
	foreach ($equipment as $equipmentid){
		mysql_query("
		INSERT INTO bookings_equipment_booked ( 
			booking_id , equipment_id
		) 
		VALUES(
			'$bookingid', '$equipmentid'
		)
		");
	}
}

function getFirstName($userid){
	$sql = "SELECT * FROM `xtvtest_wp_usermeta` WHERE `user_id` = '$userid'";
	$result = mysql_query( $sql );
	while($row = mysql_fetch_array($result)) {
			if ($row['meta_key'] == "first_name"){
				$first_name = $row['meta_value'];
			}
			if (isset($first_name)){
				break;
			}
		}
	return $first_name;
}

function getEmail($userid){
	$sql = "SELECT * FROM `xtvtest_wp_users` WHERE  `ID` = '$userid'";	
	$result = mysql_query( $sql );
	$row = mysql_fetch_array($result, MYSQL_ASSOC);
	
	return $row['user_email'];
}

function getLastName($userid){
	$sql = "SELECT * FROM `xtvtest_wp_usermeta` WHERE `user_id` = '$userid'";
	$result = mysql_query( $sql );
	while($row = mysql_fetch_array($result)) {
			if ($row['meta_key'] == "last_name"){
				$last_name = $row['meta_value'];
			}
			if (isset($last_name)){
				break;
			}
		}
	return $last_name;
}

function getFullName($userid){
	return getFirstName($userid)." ".getLastName($userid);
}

function getWeekOfDate($year, $month, $day){
	$timestamp = mktime(0, 0, 0, $month, $day, $year);
	return date("W", $timestamp);
}

function increaseTimestampByADay($timestamp){
	return $timestamp + (24 * 60 * 60);
}

function getEquipmentBookedPlain($bookingid){
	$bookedEquipment = " ";
	$sql = "SELECT * FROM `bookings_equipment_booked` WHERE  `booking_id` = '$bookingid'";	
	$result = mysql_query( $sql );
	while($row = mysql_fetch_array($result))
		{
			$bookedEquipment=$bookedEquipment.getEquipmentName($row['equipment_id']).", ";
		}
	return $bookedEquipment;
	
}

function getTimeBookedFrom($booking_id){
	$sql = "SELECT * FROM `bookings_bookings` WHERE `id` = '$booking_id'";	
	$result = mysql_query( $sql );
	$booking = mysql_fetch_array($result, MYSQL_ASSOC);	
	return  $booking['time_start'];
}

function getBookingTitle($booking_id){
	$sql = "SELECT * FROM `bookings_bookings` WHERE `id` = '$booking_id'";	
	$result = mysql_query( $sql );
	$booking = mysql_fetch_array($result, MYSQL_ASSOC);	
	return  $booking['title'];
}

function getBookingDate($booking_id){
	$sql = "SELECT * FROM `bookings_bookings` WHERE `id` = '$booking_id'";	
	$result = mysql_query( $sql );
	$booking = mysql_fetch_array($result, MYSQL_ASSOC);	
	return  $booking['date_booking'];
}

function getTimeBookedUntil($booking_id){
	$sql = "SELECT * FROM `bookings_bookings` WHERE `id` = '$booking_id'";	
	$result = mysql_query( $sql );
	$row = mysql_fetch_array($result, MYSQL_ASSOC);	
	return  $row['time_end'];
}

function getBookedUserID($booking_id){
	$sql = "SELECT * FROM `bookings_bookings` WHERE `id` = '$booking_id'";	
	$result = mysql_query( $sql );
	$row = mysql_fetch_array($result, MYSQL_ASSOC);	
	return  $row['booked_by'];
}

function getEquipmentBookings($timestamp, $equipment_id){
	$bookings = new ArrayObject(array());
	
	//get booking_id for all bookings of $equipment_id
	$sql1 = "SELECT * FROM `bookings_equipment_booked` WHERE  `equipment_id` = '$equipment_id'";	
   	$result1 = mysql_query( $sql1 );
	while($row1 = mysql_fetch_array($result1)) {
		$booking_id = $row1['booking_id'];
		
		//get bookings
		$sql2 = "SELECT * FROM `bookings_bookings` WHERE  `id` = '$booking_id'";	
		$result2 = mysql_query( $sql2 );
		
		//check if date of booking is equal to $timestamp
		while($row2 = mysql_fetch_array($result2)) {
			if($row2['date_booking'] == date("Y-m-d", $timestamp)){
				$bookings->append($row2['id']);	
			}
		  }
		}
	return $bookings;
}

function isEquipmentBookedOnDay($timestamp, $equipment_id){ //for use with weekview
	//get booking_id for all bookings of $equipment_id
	$sql1 = "SELECT * FROM `bookings_equipment_booked` WHERE  `equipment_id` = '$equipment_id'";	
   	$result1 = mysql_query( $sql1 );
	while($row1 = mysql_fetch_array($result1)) {
		$booking_id = $row1['booking_id'];
		
		//get bookings
		$sql2 = "SELECT * FROM `bookings_bookings` WHERE  `id` = '$booking_id'";	
		$result2 = mysql_query( $sql2 );
		
		//check if date of booking is equal to $timestamp
		while($row2 = mysql_fetch_array($result2)) {
			if($row2['date_booking'] == date("Y-m-d", $timestamp)){
				return True;		
			}
		  }
		}
	//if not booked
	return False;
}

function isEquipmentBooked($equipmentid, $timestamp1, $timestamp2){ //to evaluate booking
	$date = date("Y-m-d", $timestamp1);
	//look for bookings on date
	$sql = "SELECT * FROM `bookings_bookings` WHERE  `date_booking` = '$date'";	
	$result = mysql_query( $sql );
	while($row = mysql_fetch_array($result))
		{
			$booking_id = $row['id'];
			//check if time from $timestamp falls into range from booking
				$timestamp_start = strtotime($date." ".getTimeBookedFrom($booking_id));
				$timestamp_end = strtotime($date." ".getTimeBookedUntil($booking_id));
				if (
				(($timestamp1 < $timestamp_start) and ($timestamp2 <= $timestamp_start)) or
				(($timestamp1 >= $timestamp_end) and ($timestamp2 > $timestamp_end))
				){
					continue; //not booked, so restart loop with other times that day
				}
			//check for equipment in booking
			$sql2 = "SELECT * FROM `bookings_equipment_booked` WHERE  `booking_id` = '$booking_id'";	
			$result2 = mysql_query( $sql2 );
			while($row2 = mysql_fetch_array($result2)){
				//if booking contains equipment requested
				if ($row2['equipment_id'] == $equipmentid){
					return true; //is booked
				}
			}
		}
	
	return false;
}


function isTime2Later($time1, $time2){ //time in format hh:mm*
	$hour1 = substr($time1, 0, 2);
	$hour2 = substr($time2, 0, 2);
	$minute1 = substr($time1, 3, 2);
	$minute2 = substr($time2, 3, 2);
	//time1 equal
		if (($hour1 == $hour2) && ($minute1 == $minute2)){
			return false;
		}
	//time1 equal or earlier
		elseif (($hour1 <= $hour2) && ($minute1 <= $minute2)){
			return true;
		}
	//time earlier
		elseif ($hour1 < $hour2){
			return true;
		}
		else {
			return false;
		}
}



function getEquipmentName($equipment_id) {
	$sql = "SELECT * FROM `bookings_equipment` WHERE  `id` = '$equipment_id'";	
	$result = mysql_query( $sql );
	$equipment = mysql_fetch_array($result, MYSQL_ASSOC);
	
	return  $equipment['name'];
}

function getTypeName($type_id) {
	$sql = "SELECT * FROM `bookings_types` WHERE `id` = '$type_id'";	
	$result = mysql_query( $sql );
	$type = mysql_fetch_array($result, MYSQL_ASSOC);	
	return  $type['name'];
}

function isBookingApproved($booking_id){
	$sql = "SELECT * FROM `bookings_bookings` WHERE  `id` = '$booking_id'";
	$result = mysql_query( $sql );
	$row = mysql_fetch_array($result, MYSQL_ASSOC);	
	if ($row['approved'] == 1){
		return true;
	}
	else {
		return false;
	}
}

function approveBooking($booking_id){
	$sql = "UPDATE `bookings_bookings` SET  `approved` =  '1' WHERE  `id` = '$booking_id'";
	mysql_query( $sql );
}

function disapproveBooking($booking_id){
	$sql = "UPDATE `bookings_bookings` SET  `approved` =  '0' WHERE  `id` = '$booking_id'";
	mysql_query( $sql );
}

function deleteBooking($booking_id){
	$sql = "DELETE FROM `bookings_bookings` WHERE  `id` = '$booking_id'";
	mysql_query( $sql );
	
	//delete equipment booked
	$sql = "DELETE FROM `bookings_equipment_booked` WHERE  `booking_id` = '$booking_id'";
	mysql_query( $sql );
}

function getBookedByID($booking_id){
	$sql = "SELECT * FROM `bookings_bookings` WHERE `id` = '$booking_id'";	
	$result = mysql_query( $sql );
	$booking = mysql_fetch_array($result, MYSQL_ASSOC);	
	return  $booking['booked_by'];
}

function getGroupName($group_id) {
	$sql = "SELECT * FROM `bookings_groups` WHERE `id` = '$group_id'";	
	$result = mysql_query( $sql );
	$group = mysql_fetch_array($result, MYSQL_ASSOC);	
	return  $group['name'];
}

function getTypeIDfromGroupID($group_id) {
	$sql = "SELECT * FROM `bookings_groups` WHERE `id` = '$group_id'";	
	$result = mysql_query( $sql );
	$group = mysql_fetch_array($result, MYSQL_ASSOC);	
	return  $group['type'];
}

function getTypeIDfromEquipmentID($equipment_id) {
	$sql = "SELECT * FROM `bookings_equipment` WHERE  `id` = '$equipment_id'";	
	$result = mysql_query( $sql );
	$equipment = mysql_fetch_array($result, MYSQL_ASSOC);
	
	$type_id = getTypeIDfromGroupID($equipment['group']);
	return getTypeName($type_id);
}


function getNumberOfRows($table){
	$result = mysql_query("SELECT * FROM $table");
	return mysql_num_rows($result);
	}
	
function getNumberOfEquipmentBooked($bookingid){
	$result = mysql_query("SELECT * FROM bookings_equipment_booked WHERE booking_id = $bookingid");
	return mysql_num_rows($result);
	}

php?>